package br.com.mauda.bilhetesAereos.bc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.mauda.bilhetesAereos.bc.crud.TesteCiaAereaBC;

/**
 * Essa classe realizara os testes da camada BC de uma so vez.
 * Para que tudo ocorra com sucesso, a base devera estar vazia, pois existem metodos no
 * pacote Queries que necessitam que o numero de instancias da classe X esteja igual ao
 * numero descrito no Enum de testes da classe X.
 * Para limpar a base basta deletar os arquivos que se encontram na pasta banco e executar
 * novamente o script do banco de dados.
 * 
 * @author Mauda
 *
 */

@RunWith(Suite.class)
@SuiteClasses({
	TesteCiaAereaBC.class
})

public class RunnerTestesBC {
	
}
